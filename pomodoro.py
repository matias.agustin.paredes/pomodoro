from plyer import notification as noti
from setproctitle import setproctitle
import os 
import time as t

setproctitle ("pomodoro")

#Get file's directory
string = os.path.realpath (__file__)

MENSAJE = ""
ICON = string[:-12] + "/clock.ico"

class PomodoroTimer:
	def __init__(self):
		self.current_pause = 0

	#Donde la magia ocurre
	def execute (self):
		pomodoros = 1
		while True:
			MENSAJE = "Empieza tu pomodoro"
			noti.notify (title = "Pomodoro", message = MENSAJE, app_icon = ICON, timeout = 10)
			t.sleep (1500)

			MENSAJE = "Empieza tu descanso"
			noti.notify (title = "Pomodoro", message = MENSAJE, app_icon = ICON, timeout = 10)

			if pomodoros % 4 == 0:
				self.current_pause = 1800
			else:
				self.current_pause = 300

			t.sleep (self.current_pause)
			pomodoros += 1

if __name__ == "__main__":
    a = PomodoroTimer ()
    a.execute()
